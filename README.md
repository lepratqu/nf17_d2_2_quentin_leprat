# Projet NF17 : Gestion d'une agence touristique

 - Sujet : Gestion d'une agence touristique
 - Chargé de TD : Alessandro Victorino
 - Semestre : P20 
 
 ## Élève 
 
 - Quentin Leprat
 
 ## Contenu du livrable 

| Étape | D2 Mar 13h15 | Date de livraison prévue |
| ------ | ------ | ------ |
| 1 NDC - MCD | 19 Mai | Dimanche 24 Mai à 12h |
| 2 MLD,SQL CREATE et INSERT| 26 Mai | Dimanche 31 Mai à 12h |
| 3 SQL SELECT| 2 Juin | Dimanche 7 Juin à 12h |
| 4 Révision complète du projet (v2) | 9 Juin | Dimanche 14 Juin à 12h |
| 5 Révision complète du projet (v3)| semaine des finaux | Dimanche 21 Juin à 12h |